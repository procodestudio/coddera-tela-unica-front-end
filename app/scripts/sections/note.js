(function (window) {

  let cachedNotes;

  window.requestNotes = function (msisdn) {
    return fetch(`${window.ENVS.OI_TOTAL_BACKEND_BASE}/bloco-notas?movel=${msisdn || ''}&fixo=${msisdn || ''}&cachedrop=${new Date().getTime()}`, {
      headers: {
        'Pragma': 'No-Cache',
        'Cache-Control': 'No-Cache'},
    })
      .then(responseHandler)
      .then(res => res.json())
      .then(json => {
        if (!cachedNotes) {
          $('.jsNoteButton').on('click', showNotesDialog);
        }

        $('.jsNoteButton').html(`
          <img class="toolbar--action--icon" src="images/note-multiple.png">
          <strong class="toolbar--action--title">Notas</strong>
        `);

        cachedNotes = json && json.entity;
      });
  };

  function showNotesDialog() {
    let htmlNotes = cachedNotes && cachedNotes.map(note => (
      `<strong class="l-text-discreet">${note.dataRegistro}</strong>
         <p>${note.descricao}</p>`
    )).join('');

    $.confirm({
      title: '<strong>Bloco de Notas</strong>',
      theme: 'light notes',
      content: `
        <div style="max-height: 350px; overflow: auto;">${htmlNotes}</div>
        <textarea class="l-width-100 form--text-area" 
                class="jsNoteInput"
                rows="5"
                onkeyup="return limitChars(event, 400)"
                placeholder="Digite aqui sua nota"></textarea>
      `,
      buttons: {
        'SALVAR': {
          action: function () {
            let note = this.$content.find('textarea').val().trim();
            let msisdn = queryObject.get().msisdn;

            if ((note || '').length > 400) {
              $.alert('Não é possível salvar nota, máximo 400 carácteres');
              return false;
            }

            if (note.replace(/\n/g, '')) {
              let reqOpts = {
                method: 'POST',
                headers: {
                  'Content-Type': 'application/json',
                  'Pragma': 'No-Cache',
                  'Cache-Control': 'No-Cache'
                },
                body: JSON.stringify({
                  msisdn: msisdn,
                  descricao: note
                })
              };
              fetch(`${window.ENVS.OI_TOTAL_BACKEND_BASE}/bloco-notas`, reqOpts)
                .then(responseHandler)
                .then(res => {
                  $.confirm({
                    title: null,
                    content: 'Nota salva com sucesso',
                    buttons: {
                      'OK': {
                        action: function () {
                          showNotesDialog();
                        }
                      }
                    }
                  });
                  window.requestNotes(msisdn).then();
                })
                .catch(err => {
                  console.error(err);
                  $.alert('Falha ao salvar nota');
                })
            } else {
              $.alert('Não é possível salvar nota vazia');
              return false;
            }

          }
        },
        'FECHAR': {}
      }
    });
  }

  window.saveNewNote = function (nextNote) {

    if (!nextNote || !nextNote.trim()) {
      return $.alert('Não é possível salvar uma nota vazia');
    }

    let reqOpts = {
      method: 'POST',
      body: JSON.stringify({
        msisdn: queryParams.msisdn,
        descricao: nextNote
      })
    };

    fetch(`${window.ENVS.OI_TOTAL_BACKEND_BASE}/bloco-notas`, reqOpts)
      .then(responseHandler)
      .then(_ => $.alert('Nota salva com sucesso'))
      .catch(err => {
        console.error(err);
        $.alert('Falha inesperada ao salvar nota');
      });
  }

})(window);
