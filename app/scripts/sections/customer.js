function showCustomerPage() {
  $('.client--personal').insertBefore('.client--general');
  $('.js-page').hide();
  $('.js-page.js-page-client').show();
  $('.js-massive').show();
}

function buildCustomerSection(clientJson, hasFakePhone, ucid, msisdn, fetchOptions) {
  let hasFraud,
    hasPadLock,
    hasFinancialBlockage,
    numerosDependente = clientJson.numerosDependente;

  if (numerosDependente && numerosDependente.length > 1) {
    $('.jsDependentsContainer')
      .attr('data-tooltip', 'Clique para exibir todos')
      .addClass('l-clickable')
      .on('click', _ => $.alert({
        title: 'Móveis de Dependentes',
        content: '<ul>' + numerosDependente.map(it => `<li>${it}</li>`).join('') + '</ul>',
        boxWidth: '500px'
      }));
  }

  if (clientJson.protocolo && clientJson.numMovel) {
    prepareSmsDialog(hasFakePhone, clientJson, numerosDependente, msisdn, ucid, fetchOptions);
  } else {
    $('.jsMessageIcon').hide();
  }

  hasFinancialBlockage = clientJson.bloqFinanc != 'SEM BLOQUEIO' ||
    clientJson.bloqSinn != 'Não' ||
    clientJson.bloqStcSac != 'Não';

  hasFraud = clientJson.fraude || clientJson.fraudeSinn || clientJson.fraudeStcTac;
  hasPadLock = clientJson.elegivelDesbloqueio;

  let jsPlan = clientJson.plano;
  let jsOffer = (clientJson.oferta && clientJson.oferta.nomeOferta);
  let jsPlanTv = clientJson.planoTv;

  populateClientView({
    jsCpf: clientJson.cpf,
    jsBirth: formatDate(clientJson.dataNasc),
    jsPlan: ` <strong>Plano Oi Total </strong> <br/> <span class="l-text-discreet">${jsPlan || ''} </span> `,
    jsOffer: ` <strong>Oferta </strong> <br/> <span class="l-text-discreet">${jsOffer || ''} </span> `,
    jsPlanTv: ` <strong>Plano TV </strong> <br/> <span class="l-text-discreet">${jsPlanTv || ''} </span> `,
    jsName: (clientJson.nomeCliente || '').trim(),
    jsFraud: `<img class="icon" src="images/${hasFraud ? 'ch' : 'unch'}eckedbox.png">
              ${hasFraud ? '<img class="icon" src="images/info.png">' : ''}`,
    jsBundleType: clientJson.tipoBundle,
    jsTvContract: clientJson.contratoTv,
    jsFixo: clientJson.numFixo,
    jsMobile: clientJson.numMovel || '',
    jsDependents: (numerosDependente && numerosDependente[0]) || '',
    jsDependentsCount: (numerosDependente && numerosDependente.length) || '',
    jsfinancialBlockage: `<img class="icon" src="images/${hasFinancialBlockage ? 'ch' : 'unch'}eckedbox.png">
                           ${hasFinancialBlockage ? '<img class="icon" src="images/info.png">' : ''}`,
    jsfinancialBlockageEnabled: hasFinancialBlockage ? hasPadLock ? 'Sim' : 'Não' : '-',
    jspadlock: '<img class="icon" style="width: auto; height: 17px;" src="images/icons/padlock.png">',
    jsProtocol: clientJson.protocolo,
  });

  if (hasFraud) {
    $('.jsFraudContainer').addClass('l-clickable').on('click', e => {
      showFraudDialog({
        fraude: clientJson.fraude,
        fraudeSinn: clientJson.fraudeSinn,
        fraudeStcTac: clientJson.fraudeStcTac
      })
    })
  }

  if (hasPadLock) {
    $('.jspadlock')
      .css({display: 'inline-block'})
      .addClass('l-clickable')
      .on('click', e => {
        showPadlockDialog({msisdn: msisdn, ucid: ucid})
      })
  } else {
    $('.jspadlock').css({display: 'none'});
  }

  if (hasFinancialBlockage) {
    $('.jsfinancialBlockage').addClass('l-clickable').on('click', e => {
      showFinancialBlockageDialog({
        bloqFinanc: clientJson.bloqFinanc,
        bloqSinn: clientJson.bloqSinn,
        bloqStcSac: clientJson.bloqStcSac
      });
    })
  }

  if (hasFakePhone) {
    $('.jsDependentsContainer').css('visibility', 'hidden');
    populateClientView({jsMobileLabel: 'Nº Siebel'});
  } else {
    $('.jsDependentsContainer').css('visibility', 'visible');
    populateClientView({jsMobileLabel: 'Móvel'});
  }

  runTooltipFallbackIfNeeded();
}

function showPadlockDialog({msisdn, ucid}) {
  const requestInfo = {
    method: 'GET',
    headers: new Headers({
      'Content-type': 'application/json',
      'pragma': 'no-cache',
      'cache-control': 'no-cache'
    })
  };

  $.confirm({
    title: 'Desbloqueio',
    content: 'Deseja solicitar o desbloqueio em confiança?',
    buttons: {
      Sim: function Sim() {
        fetch(window.ENVS.OI_TOTAL_BACKEND_BASE + `/desbloqueio?ani=${clientJson.numMovel}&ucid=${ucid}&msisdn=${msisdn}&sessionId=${utilsData.activeSession}`, requestInfo).then(function (res) {
          return res.json();
        }).then(function (json) {
          if (json.entity.codRetorno === '10000') {
            $.alert('<span style=\'font-size: 22px;\'>Desbloqueio</span><br/>Solicitação registrada com sucesso. Aguarde o prazo de até 24h para o sistema concluir o desbloqueio em confiança. Lembrando que, como este plano possui mais de '
              + 'um produto associado, os serviços voltarão a funcionar em momentos diferentes.');
          } else {
            $.alert('Não foi possível realizar a solicitação de desbloqueio');
          }
          return true;
        })['catch'](function (e) {
          return console.error(e);
        });

      },
      Não: {}
    }
  });
}

function showFinancialBlockageDialog({bloqFinanc, bloqSinn, bloqStcSac}) {
  let siebel = '',
  stcSac = '',
  sinn = '';

  switch(bloqFinanc) {
    case 'SEM BLOQUEIO':
      siebel = 'Não';
      break;
    case 'BLOQUEIO PARCIAL':
      siebel = 'Sim <span class="chip isYellow">parcial</span>';
      break;
    case 'BLOQUEIO TOTAL':
      siebel = 'Sim <span class="chip isRed">total</span>';
      break;
  }

  switch(bloqStcSac) {
    case 'Não':
      stcSac = 'Não';
      break;
    case 'Sim':
      stcSac = '<strong class="chip">Sim</strong>';
      break;
    case 'Sim Parcial':
      stcSac = 'Sim <span class="chip isYellow">parcial</span>';
      break;
    case 'Sim Total':
      stcSac = 'Sim <span class="chip isRed">total</span>';
      break;
  }

  switch(bloqSinn) {
    case 'Não':
      sinn = 'Não';
      break;
    case 'Bloqueio Total':
      sinn = '<strong class="chip">Sim</strong>';
      break;
  }

  $.alert({
    theme: 'light no-overflow',
    title: 'Bloqueios',
    content: `<div class="l-margin-15 l-field">
                <span class="l-text-discreet">Bloqueio Siebel</span>
                <span class="jsBundleType l-float-right">${siebel}</span>
              </div>
              <div class="l-margin-15 l-field">
                <span class="l-text-discreet">Bloqueio SINN</span>
                <span class="v l-float-right">${sinn}</span>
              </div>
              <div class="l-margin-15 l-field">
                <span class="l-text-discreet">Bloqueio STC/SAC</span>
                <span class="jsBundleType l-float-right">${stcSac}</span>
              </div>`
  });
}

function showAddonDialog(addons) {
  $.alert({
    theme: 'light addons',
    title: '<div></div>',
    content: `
              <table class="table-addon">
                <thead>
                  <tr>
                    <th class="l-inline-left">Plano Add-On Oi Total Play</th>
                    <th class="l-inline-right">Data Ativação</th>
                  </tr>
                </thead>
                <tbody>
                  ${ addons.map(addon => `
                    <tr>
                      <td class="l-inline-left">${addon.plan}</td>
                      <td class="l-inline-right">${addon.data_ativacao}</td>
                    </tr>
                    `).join('')
      }
                </tbody>
              </table>
            `
  });
}

function showFraudDialog({fraude, fraudeSinn, fraudeStcTac}) {
  $.alert({
    theme: 'light no-overflow',
    title: 'Fraudes',
    content: `<div class="l-margin-15 l-field">
                <span class="l-text-discreet">Fraude Siebel</span>
                <span class="jsBundleType l-float-right">${fraude ? 'Sim' : 'Não'}</span>
              </div>
              <div class="l-margin-15 l-field">
                <span class="l-text-discreet">Fraude SINN</span>
                <span class="jsBundleType l-float-right">${fraudeSinn ? 'Sim' : 'Não'}</span>
              </div>
              <div class="l-margin-15 l-field">
                <span class="l-text-discreet">Fraude STC/SAC</span>
                <span class="jsBundleType l-float-right">${fraudeStcTac ? 'Sim' : 'Não'}</span>
              </div>`
  })
}

function requestAddons(cpf, ucid, msisdn, fetchOptions) {
  return fetch(`${window.ENVS.OI_TOTAL_BACKEND_BASE}/addon/${cpf}?ucid=${ucid}&msisdn=${msisdn}&sessionId=${utilsData.activeSession}`, fetchOptions)
    .then(responseHandler)
    .then(res => res.json())
    .then(json => {
      Monitoring.changeStatus('addon');

      if (json.status != 200) {
        return populateClientView({jsAddon: 'Não'});
      }

      let addons = json.entity && json.entity.services;

      if (addons && addons.length > 0) {
        populateClientView({jsAddon: 'Sim'});
        $('.jsAddonIcon').css('display', '');
        $('.jsAddonContainer').addClass('l-clickable').on('click', _ => showAddonDialog(addons));
      } else {
        populateClientView({jsAddon: 'Não'});
      }
    });
}

function showClientInvalidMessage(content) {
  showFullMessage('.dash.l-relative', '');
  showFullMessage('.jsInvoice', '');
  showFullMessage('.massive-failure.l-relative', '');
  showFullMessage('.client.l-relative', '');
  showFullMessage('.jsOs.l-relative', '');
  $('.jsInvalidClientMessage').append(content);
}

function showErrorMessage(content) {
  $('.jsInvalidClientMessage').append(content);
}

function prepareSmsDialog(hasFakePhone, clientJson, numerosDependente, msisdn, ucid, fetchOptions) {
  $('.jsMessageIcon').on('click', _ => {

    let customNumberInput, dialogOptions = [];

    if (hasFakePhone) {
      customNumberInput = `<label class="l-block l-clickable">
           <input class="form--radio" type="radio" name="phone" value="CUSTOM">
           <span>Outro Número: </span>
           <input class="jsInput form--no-spin" placeholder="00000000000" type="number" style="width: 16ch">
         </label>`;
    } else {
      dialogOptions.push({label: 'Titular', phone: clientJson.numMovel});
    }

    if (numerosDependente) {
      dialogOptions.push(
        ...numerosDependente.map((phone, index) => ({label: `Dep. ${index + 1}`, phone}))
      )
    }

    if (hasFakePhone && clientJson.numeroBinado && clientJson.numeroBinado.length > 10) {
      dialogOptions.push({label: 'Binado', phone: clientJson.numeroBinado});
    }

    const dialogContent = `
          <form class="form jsProtocolForm" onsubmit="return false">
            ${[...dialogOptions.map(op =>
      `<label class="l-block l-clickable">
                    <input class="form--radio" type="radio" name="phone" value="${op.phone}">
                    <span>${op.label}: ${op.phone}</span>
                 </label>`),
      customNumberInput
    ].filter(it => it).join('')
      }
          </form>`;

    $.confirm({
      title: 'Envio de Protocolo',
      content: dialogContent,
      onContentReady: function () {
        this.$content.find('.jsProtocolForm').on('submit', e => e && e.preventDefault && e.preventDefault());
        this.$content.find('.jsInput').numeric();
        this.$content.find('.form').on('click', e => {
          const selectedPhone = this.$content.find('form').serializeArray().find(it => it.value);

          if (selectedPhone && selectedPhone.value === 'CUSTOM') {
            this.$content.find('.jsInput').css('display', 'inline-block');
          } else {
            this.$content.find('.jsInput').css('display', 'none');
          }
        });
      },
      buttons: {
        Cancelar: {},
        Enviar: {
          action: function () {
            const selectedPhone = this.$content.find('form').serializeArray().find(it => it.value);
            if (selectedPhone && selectedPhone.value) {

              let destination = selectedPhone.value === 'CUSTOM' ? destination = this.$content.find('.jsInput').val() : selectedPhone.value;

              if (destination.length < 11 || destination.length > 11) {
                $.alert(`O número <strong>${destination}</strong> não possui 11 digitos`);
                return false;
              }

              if (destination[2] != 9) {
                $.alert(`O 3º digito do número <strong>${destination}</strong><br/> deve ser 9`);
                return false;
              }

              fetch(`${window.ENVS.OI_TOTAL_BACKEND_BASE}/envia-protocolo?ani=${destination}&protocolo=${clientJson.protocolo}&data=${new XDate().toString('dd/MM/yyyy')}&ucid=${ucid}&msisdn=${msisdn}&sessionId=${utilsData.activeSession}`, fetchOptions)
                .then(res => res.json())
                .then(res => {
                  if (res.entity && res.entity.code && +res.entity.code === 0) {
                    $.alert({
                      title: 'SMS',
                      content: `Protocolo enviado com sucesso por SMS para ${destination}`
                    });
                  } else {
                    $.alert({
                      title: 'SMS',
                      content: `Não foi possível enviar o SMS para o número <strong>${destination}</strong>`
                    });
                  }
                });
            } else {
              $.alert('Selecione um número para envio de protocolo');
              return false;
            }
          }
        }
      }
    });
  });
}
