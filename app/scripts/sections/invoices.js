const invoicesUtils = {};

function showInvoicesPage() {
  $('.client--personal').appendTo('.js-page-invoices .jsClientContainer');
  $('.js-page').hide();
  $('.js-page.js-page-invoices').show();
}

function requestInvoices(clientJson, hasFakePhone, ucid, msisdn, fetchOptions) {
  let cutDate = clientJson.dataCorte;
  invoicesUtils.msisdn = msisdn;
  invoicesUtils.ucid = ucid;

  fetch(`${window.ENVS.OI_TOTAL_BACKEND_BASE}/faturas?numMovel=${clientJson.numMovel || ''}&fixo=${clientJson.numFixo || ''}&contratoFixo=${clientJson.contratoFixo}&contratotv=${clientJson.contratoTv}&ucid=${ucid}&msisdn=${msisdn}&sessionId=${utilsData.activeSession}&dataDeCorte=${cutDate}`, fetchOptions)
    .then(responseHandler)
    .then(res => res.json())
    .then((invoicesResponse = {}) => {

      //SIEBEL
      let siebelInvoices = invoicesResponse && invoicesResponse.faturaMovel.filter(it => it);
      if (siebelInvoices.length === 0) showFullMessage('.jsInvoiceSiebel', 'Não há faturas disponíveis.');

      let siebelItems = siebelInvoices.map((invoice, index) => {
        let tableItem = buildSiebelItem(invoice, clientJson.numMovel, msisdn, index);
        tableItem.find('.jsSendProtocol.l-clickable').on('click', _ => {
          sendBarcodeViaSMS(clientJson, invoice.numBoleto, invoice.valor.toFixed(2), invoice.vencimento, hasFakePhone, msisdn, ucid, 'mobile');
        });
        tableItem.find('.jsSendInvoiceCopy.l-clickable').on('click', _ => showInvoiceCopyDialog(invoice, clientJson, msisdn, ucid, fetchOptions));
        tableItem.find('.jsShowMsgContest.l-clickable').on('click', _ => {
          showOptSendResulContsViaSMS(invoice, clientJson, hasFakePhone, msisdn, ucid, 'mobile', 'row');
        });
        return tableItem;
      });

      //FIXO
      let fixoInvoices = invoicesResponse.faturaFixo;
      let sorter = (a, b) => moment(b.vencimento, 'DD/MM/YYYY').isBefore(moment(a.vencimento, 'DD/MM/YYYY')) ? 0 : 1;
      let fixoItems = fixoInvoices.filter(it => it.pago === false && it.valor).sort(sorter).map((invoice, index) => {
        let tableItem = buildFixoItem(invoice, clientJson.numMovel, msisdn, index);
        tableItem.find('.jsSendProtocol.l-clickable').on('click', _ => {
          let barcodeParts = formatBarcodes(invoice.codigoBarras);
          let formattedBarcode = `${barcodeParts[0]} ${barcodeParts[1]} ${barcodeParts[2]} ${barcodeParts[3]}`;
          sendBarcodeViaSMS(clientJson, formattedBarcode, invoice.valor.toFixed(2), invoice.vencimento, hasFakePhone, msisdn, ucid, 'phone');
        });
        return tableItem;
      });

      if (fixoItems.length === 0) showFullMessage('.jsInvoiceFixo', 'Não há faturas disponíveis.');

      //TV
      let tvItems = invoicesResponse.faturaTV.filter(it => it.dataVencimento).reverse().map((invoice, index) => {
        let tableItem = buildTvItem(invoice, clientJson.numMovel, msisdn, index);
        tableItem.find('.jsSendProtocol.l-clickable').on('click', _ => {
          let barcodeParts = formatBarcodes(invoice.codigoBarrasConta);
          let formattedBarcode = `${barcodeParts[0]} ${barcodeParts[1]} ${barcodeParts[2]} ${barcodeParts[3]}`;
          sendBarcodeViaSMS(clientJson, formattedBarcode, (+invoice.valorConta).toFixed(2), invoice.dataVencimento, hasFakePhone, msisdn, ucid, 'tv');
        });
        return tableItem;
      });
      if (tvItems.length === 0) showFullMessage('.jsInvoiceTv', 'Não há faturas disponíveis.');

      tvItems.forEach(it => $('#tvInvoiceContainer tbody').append(it));
      fixoItems.forEach(it => $('#phoneInvoiceContainer tbody').append(it));
      siebelItems.forEach(it => $('#siebelInvoiceContainer tbody').append(it));

      setupClipboard();
      removeFullLoading('.jsInvoice');
      runTooltipFallbackIfNeeded();
      $('.cut-date').html(cutDate);

      if (fixoInvoices.find(it => (it || {}).regiao == 'R2')) {
        $('.invoiceTitle').html('Faturas em Aberto - SAC');
        $('.jsShouldHideForR2').hide();
        $('#phoneInvoiceContainer tbody .jsSendProtocol').hide();
      } else {
        $('.invoiceTitle').html('Faturas em Aberto - STC');
      }

      if (siebelInvoices.length) {
        $('.jsShowAllMsgContest')
          .css({'display':'block'})
          .on('click', _ => {
          showOptSendResulContsViaSMS('', clientJson, hasFakePhone, msisdn, ucid, 'mobile', 'all');
        });
      }

      Monitoring.changeStatus('invoices');
    })
    .catch(e => {
      console.error('error:', e);
      showFullMessage('.jsInvoice', '<span class="l-error">Não foi possível realizar a consulta<br/> no momento</span>');
      Monitoring.changeStatus('invoices');
    });
}

function formatBarcodes(barcodeStr = '') {
  if (!barcodeStr) return '';
  let rawBarcode = barcodeStr.replace(/\D/g, '');
  let match = /(\d{12})(\d{12})(\d{12})(\d{12})/.exec(rawBarcode);
  let parts = [match[1], match[2], match[3], match[4]];
  return parts.map(it => {
    let match = /(\d{11})(\d{1})/.exec(it);
    return `${match[1]}-${match[2]}`
  })
}

function showCopiedMessage(element) {
  let original = $(element).attr('data-barcode');
  let failureMessage = 'Browser não suportado\ncopie manualmente';
  let message;

  try {
    message = !Clipboard || !Clipboard.isSupported() ? failureMessage : 'Copiado!\n(Ctrl+V para colar)';
  } catch (e) {
    //console.warn('Cannot copy with clipboard.js', e);
    let barcodeParts = formatBarcodes(original);
    $.alert({
      title: 'Código de Barras',
      content: `${barcodeParts[0]} ${barcodeParts[1]}<br/>${barcodeParts[2]} ${barcodeParts[3]}`
    });
  }

  setTimeout(_ => $(element).attr('data-tooltip', original), 1500);
  $(element).attr('data-tooltip', message || failureMessage);
}

function buildSiebelItem(invoice, numMovel, msisdn, invoiceIndex) {
  let rawBarCode = (invoice.numBoleto || '').replace(/\D/g, '');
  let barcodeParts = formatBarcodes(invoice.numBoleto);
  let tableItem = `
        <tr>
          <td>${invoice.vencimento || '-'}</td>
          <td class="l-inline-center">
            <img class="icon" src="images/${invoice.flagFaturaPaga ? '' : 'un'}checkedbox.png"/>
          </td>
          <td class="l-siebel-field">${ invoice.valorFaturaOriginal ? 'R$' : '' } ${dashOrNumber(invoice.valorFaturaOriginal)}</td>
          <td class="l-siebel-field">${ invoice.valorConcedido ? 'R$' : ''} ${dashOrNumber(invoice.valorConcedido)}</td>
          <td>${ invoice.valor ? 'R$' : ''} ${dashOrNumber(invoice.valor) || '-'}</td>
          <td class="l-siebel-field">${invoice.numFatura}</td>
          <td class="l-siebel-field">${invoice.numContaFatura}</td>
          <td>
            <span class="tooltip-up" ${tooltipForContestation(invoice)}>
              <img class="icon" src="images/${hasContestation(invoice.contestacao) ? '' : 'un'}checkedbox.png"/>   
            </span>   
          </td>
          <td>
            <span class="l-clickable tooltip-up jsShowMsgContest isVisible e-fade"
              data-tooltip="Enviar SMS com o resultado da contestação.">
              <img class="icon" src="images/sms_contestacao.png"/>
            </span> 
          </td>
          <td>
            <span class="l-clickable tooltip-up tooltip-paragraph js-clipboard" 
                  onclick="showCopiedMessage(this)"
                  data-barcode="${`${barcodeParts[0]} ${barcodeParts[1] + '\n'} ${barcodeParts[2]} ${barcodeParts[3]}`}"
                  data-msisdn="${msisdn}"
                  data-tooltip="${barcodeParts[0]} ${barcodeParts[1]}\n${barcodeParts[2]} ${barcodeParts[3]}"
                  data-clipboard-text="${(`${barcodeParts[0]} ${barcodeParts[1]} ${barcodeParts[2]} ${barcodeParts[3]}` || '')}">
              <img class="icon" src="images/barcode.png"/>
            </span>
            
            <div class="l-inline-block jsBarcodeSms tooltip-up jsSendProtocol e-fade isVisible ${(invoice.flagFaturaPaga && 'isPartial l-no-pointer-events' || 'l-clickable')}"
                  data-value="${invoice.valor.toFixed(2)}"
                  data-nummovel="${numMovel}"
                  data-expiredate="${invoice.vencimento}"
                  data-barcode="${rawBarCode}"
                 ${!invoice.flagFaturaPaga && 'data-tooltip="Enviar código de barras via SMS"'}>
              <img class="icon" src="images/sms_icon.png">
            </div>
          </td>
        </tr>
`;
  return $(tableItem);
}

function buildFixoItem(invoice, numMovel, msisdn, invoiceIndex) {
  let rawBarCode = (invoice.codigoBarras || '').replace(/\D/g, ''),
    formattedBarCode = formatBarcodes(rawBarCode);

  let tableItem = `
      <tr>
        <td>${invoice.vencimento}</td>
        <td class="l-inline-center">
          <img class="icon" src="images/${invoice.pago ? '' : 'un'}checkedbox.png"/>
        </td>
        <td>${ invoice.valor ? 'R$' : ''} ${dashOrNumber(invoice.valor) || '-'}</td>
        <td style="font-size: 14px">
            <span class="jsShouldHideForR2">${formattedBarCode[0]} ${formattedBarCode[1]}<br/>
            ${formattedBarCode[2]} ${formattedBarCode[3]}</span>
        </td>
        <td>
          <div class="l-inline-block jsBarcodeSms tooltip-up jsSendProtocol jsShouldHideForR2 e-fade isVisible ${(invoice.pago && 'isPartial l-no-pointer-events' || 'l-clickable')}"
                data-value="${invoice.valor.toFixed(2)}"
                data-nummovel="${numMovel}"
                data-expiredate="${invoice.vencimento}"
                data-barcode="${rawBarCode}"
               ${!invoice.flagFaturaPaga && 'data-tooltip="Enviar código de barras via SMS"'}>
            <img class="icon" src="images/sms_icon.png">
          </div>
        </td>
      </tr>
`;
  return $(tableItem);
}

function buildTvItem(invoice, numMovel, msisdn, invoiceIndex) {
  let rawBarCode = (invoice.codigoBarrasConta || '').replace(/\D/g, ''),
    formattedBarCode = formatBarcodes(rawBarCode);

  let tableItem = `
      <tr>
        <td>${invoice.dataVencimento}</td>
        <td class="l-inline-center">
          <img class="icon" src="images/${invoice.isPaga ? '' : 'un'}checkedbox.png"/>
        </td>
        <td>${ +invoice.valorConta ? 'R$' : ''} ${dashOrNumber(+invoice.valorConta) || '-'}</td>
        <td style="font-size: 14px">
            ${formattedBarCode[0]} ${formattedBarCode[1]}<br/>
            ${formattedBarCode[2]} ${formattedBarCode[3]}
        </td>
        <td>
          <div class="l-inline-block jsBarcodeSms tooltip-up jsSendProtocol e-fade isVisible ${(invoice.isPaga && 'isPartial l-no-pointer-events' || 'l-clickable')}"
                data-value="${(+invoice.valorConta).toFixed(2)}"
                data-nummovel="${numMovel}"
                data-expiredate="${invoice.vencimento}"
                data-barcode="${rawBarCode}"
               ${!invoice.isPaga && 'data-tooltip="Enviar código de barras via SMS"'}>
            <img class="icon" src="images/sms_icon.png">
          </div>
        </td>
      </tr>
`;
  return $(tableItem);
}

function showInvoiceCopyDialog(invoice, clientJson, msisdn, ucid, fetchOptions) {
  $.confirm({
    theme: 'light invoice-copy',
    title: `<span class="icon">
              <img class="icon--figure" src="images/warning.png"/>
              <span class="icon--text">CONFIRME O EMAIL NO GED</span>
            </span>`,
    content: `Confirma a solicitação de Segunda Via do 
              Valor <strong>R$ ${(invoice.valor || invoice.valorFaturaOriginal).toFixed(2)}</strong>
              com vencimento em <strong>${invoice.vencimento}</strong>?<br><br>`,
    buttons: {
      'NÃO': {},
      SIM: {
        action: _ => fetch(`${window.ENVS.OI_TOTAL_BACKEND_BASE}/segunda-via-fatura/${clientJson.numMovel}?ucid=${ucid}&msisdn=${msisdn}&sessionId=${utilsData.activeSession}`, fetchOptions)
          .then(responseHandler)
          .then(res => res.json())
          .then(res => {
            switch ((res.entity || {}).code) {
              case '10000':
                return $.alert({title: 'Mensagem', content: 'Solicitação registrada com sucesso'});
              case '10088':
                return $.alert({title: 'Mensagem', content: 'Já foi feita uma solicitação de 2ª via para esta fatura'});
              default:
                return $.alert({title: 'Mensagem', content: 'Não foi possível solicitar 2ª via'});
            }
          })
      }
    }
  });
}

function dashOrNumber(value) {
  if (value > 0) {
    return value.toFixed(2);
  } else {
    return '-';
  }
}

function hasContestation(contestation) {
  if (contestation) {
    return contestation.contestacaoAberta || contestation.contestacaoProcedente;
  }
}

function hasProperContestation(contestation) {
  return contestation.contestacaoProcedente === true;
}

function tooltipForContestation(invoice) {
  if (invoice.isCancelado || !invoice.contestacao) return;
  if (invoice.contestacao.contestacaoProcedente === true) return 'data-tooltip=\'PROCEDENTE\'';
  if (invoice.contestacao.contestacaoProcedente === false) return 'data-tooltip=\'IMPROCEDENTE\'';
  if (invoice.contestacao.contestacaoAberta === true) return 'data-tooltip=\'ABERTA\'';
}

function sendBarcodeViaSMS(clientJson, barCode, invoiceValue, invoiceExpireDate, hasFakePhone, msisdn, ucid, type) {
  smsSenderDialog(barCode, hasFakePhone, clientJson, invoiceValue, invoiceExpireDate, msisdn, ucid, type, 'barcode');
}

function showOptSendResulContsViaSMS(invoice, clientJson, hasFakePhone, msisdn, ucid, type, obj) {
  let customNumberInput;
  let dialogOptions = [];
  let resultContesPP;
  let resultContesNPP;
  let resultContesIMP;
  let contestacaoMsg;

  if (obj === 'row') {
    let barcodeParts = formatBarcodes(invoice.numBoleto);
    invoicesUtils.type = 'row';
    invoicesUtils.code = `${barcodeParts[0]} ${barcodeParts[1]} ${barcodeParts[2]} ${barcodeParts[3]}`;
  } else {
    invoicesUtils.type = 'all';
  }

  fetch(`${window.ENVS.OI_TOTAL_BACKEND_BASE}/list-resultado-contestacao`)
    .then(responseHandler)
    .then(res => res.json())
    .then(json => {
      json.forEach(element =>{
        switch (element.flag) {
          case 'PP':
            resultContesPP = element.msg;
            break;
          case 'NPP':
            resultContesNPP = element.msg;
            break;
          case 'IMP':
            resultContesIMP = element.msg;
            break;
        }
      });

      if (obj == 'all'){
        dialogOptions.push({label: 'Contestação procedente conta PAGA', msg: resultContesPP, flag: 'PP'});
        dialogOptions.push({label: 'Contestação procedente conta NÃO PAGA', msg: resultContesNPP, flag: 'NPP'});
      } else {
        switch (invoice.flagFaturaPaga) {
          case true:
            dialogOptions.push({label: 'Contestação procedente conta PAGA', msg: resultContesPP, flag: 'PP'});
            break;
          case false:
            dialogOptions.push({label: 'Contestação procedente conta NÃO PAGA', msg: resultContesNPP, flag: 'NPP'});
            break;
        }
      }

      dialogOptions.push({label: 'Contestação improcedente', msg: resultContesIMP, flag: 'IMP'});

      const dialogContent = `
        <form class="form jsProtocolForm jsShowMsgContestForm" onsubmit="return false">
          ${[...dialogOptions.map(op =>
        `<label class=" l-block l-clickable">
              <input class="form--radio" type="radio" name="pgProced" id="${op.flag}" value="${op.msg}">
                <span>${op.label}:</br>
                  <p style="text-align=justify"><em>"${op.msg}"</em></p>
                </span>
              
            </label>
            `),
        customNumberInput
      ].filter(it => it).join('')
        }
      </form>
      `;

      $.confirm({
        boxWidth: '400px',
        useBootstrap: false,
        title: 'Resultado da contestação',
        content: dialogContent,
        onContentReady: function () {
          this.$content.find('.jsShowMsgContestForm').on('change', e =>{
            contestacaoMsg = this.$content.find('form').serializeArray().find(it => it.value);
            invoicesUtils.flag = $('input[name=pgProced]:checked')[0].id;
          })
        },
        buttons: {
          Cancelar: {},
          Prosseguir: {
            action: function () {
              const selectedMsg = this.$content.find('form').serializeArray().find(it => it.value);

              if (selectedMsg && selectedMsg.value) {
                smsSenderDialog(contestacaoMsg.value, hasFakePhone, clientJson, '', '', msisdn, ucid, type, 'contest')
              } else {
                $.alert('Selecione uma opção para envio da mensagem de contestação.');
                return false;
              }
            }
          }
        }
      });
    });
}

function smsSenderDialog(msg, hasFakePhone, clientJson, invoiceValue, invoiceExpireDate, msisdn, ucid, type, key) {
  let SMSType = key;
  let confirm_title;
  let alert_title;
  let alert_Ok_content;
  let alert_notSend;
  let customNumberInput;
  let dialogOptions = [];

  if (SMSType == 'barcode') {
    confirm_title = 'Envio de Código de Barras';
    alert_title = 'SMS';
    alert_Ok_content = 'Código de Barras enviado com sucesso por SMS para ';
    alert_notSend = 'Selecione um número para envio de código de barras.'
  }

  if (SMSType == 'contest') {
    confirm_title = 'Envio de SMS Contestação';
    alert_title = 'SMS Contestação';
    alert_Ok_content = 'SMS de Contestação enviado com sucesso para  ';
    alert_notSend = 'Selecione um número para envio do resultado da contestação.'
  }

  if (hasFakePhone) {
    customNumberInput = `<label class="l-block l-clickable">
      <input class="form--radio" type="radio" name="phone" value="CUSTOM">
      <span>Outro Número: </span>
      <input class="jsInput form--no-spin" placeholder="00000000000" type="number" style="width: 16ch">
      </label>`;
  } else {
    dialogOptions.push({label: 'Titular', phone: clientJson.numMovel});
  }

  if (clientJson.numerosDependente) {
    dialogOptions.push(
      ...clientJson.numerosDependente.map((phone, index) => ({label: `Dep. ${index + 1}`, phone}))
    );
  }

  if (hasFakePhone && clientJson.numeroBinado && clientJson.numeroBinado.length > 10) {
    dialogOptions.push({label: 'Binado', phone: clientJson.numeroBinado});
  }

  const dialogContent = `<form class="form jsProtocolForm jsSendSMSContestacao" onsubmit="return false">${[...dialogOptions.map(op => `<label class="l-block l-clickable"><input class="form--radio" type="radio" name="phone" value="${op.phone}"><span>${op.label}: ${op.phone}</span></label>`), customNumberInput].filter(it => it).join('')}</form>`;

  $.confirm({
    title: confirm_title,
    content: dialogContent,
    onContentReady: function () {
      this.$content.find('.jsInput').numeric();
      this.$content.find('.form').on('click', e => {
        const selectedPhone = this.$content.find('form').serializeArray().find(it => it.value);

        if (selectedPhone && selectedPhone.value === 'CUSTOM') {
          this.$content.find('.jsInput').css('display', 'inline-block');
        } else {
          this.$content.find('.jsInput').css('display', 'none');
        }
      })
    },
    buttons: {
      Cancelar: {},
      Enviar: {
        action: function () {
          const selectedPhone = this.$content.find('form').serializeArray().find(it => it.value);

          if (selectedPhone && selectedPhone.value) {

            let destination = selectedPhone.value === 'CUSTOM' ? destination = this.$content.find('.jsInput').val() : selectedPhone.value;

            if (destination.length < 11 || destination.length > 11) {
              $.alert(`O número <strong>${destination}</strong> não possui 11 digitos.`);
              return false;
            }

            if (destination[2] != 9) {
              $.alert(`O 3º digito do número <strong>${destination}</strong><br/> deve ser 9.`);
              return false;
            }

            let url;

            if (SMSType == 'barcode') {
              url= `${window.ENVS.OI_TOTAL_BACKEND_BASE}/envia-codigo-barras?codigoBarras=${msg}&ani=${destination}&valor=${invoiceValue}&vencimento=${invoiceExpireDate}&ucid=${ucid}&type=${type}&msisdn=${msisdn}&sessionId=${utilsData.activeSession}`;
            } else {
              let flag = invoicesUtils.flag;
              let code = '';

              if (invoicesUtils.type === 'row') {
                code = `&codigoBarras=${invoicesUtils.code}`;
              }

              url = `${window.ENVS.OI_TOTAL_BACKEND_BASE}/envia-resultado-contestacao?msisdn=${msisdn}&msg=${msg}&ucid=${ucid}&ani=${destination}&sessionId=${utilsData.activeSession}&flag=${flag}${code}`;
            }

            fetch(url)
              .then(res => res.json())
              .then(res => {
                if (res.entity && res.entity.code && +res.entity.code === 0) {
                  $.alert({
                    title: alert_title,
                    content: (alert_Ok_content + destination)
                  });
                } else {
                  $.alert({
                    title: alert_title,
                    content: `Não foi possível enviar o SMS para o número <strong>${destination}</strong>.`
                  });
                }
              })

          } else {
            $.alert(alert_notSend);
            return false;
          }
        }
      }
    }
  });
}
