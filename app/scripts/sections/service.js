let services = ['Caixa Postal', 'Chamada em Espera', 'Siga-me', 'Conferência',
  'Bloq. de Cham. 0300,0500 e 0900', 'Bloqueio de Chamadas à cobrar', 'Bloqueio de Chamadas DDD',
  'Bloqueio de chamadas DDI', 'Inibidor de Número'];
let codService = ['PCS-C0204', 'PCS-H0302', 'PCS-H0325', 'PCS-H0305', 'PCS-H0306',
  'PCS-H0304', 'PCS-H0309', 'PCS-H0310', 'PCS-H0312'];
let msgInfo = ['A Caixa Postal funciona como uma secretária eletrônica.\n'
+ 'Quando você não puder ou não quiser atender sua ligação,\n'
+ 'a chamada vai cair na Caixa Postal. Para ouvir as mensagens,\n'
+ 'basta digitar *100.',
  'Se você receber outra ligação enquanto estiver\n'
  + 'conversando com alguém, você poderá colocar a\n'
  + 'pessoa com quem está falando em espera e começar\n'
  + 'a conversar com quem acabou de ligar para você.\n '
  + 'Você também pode alternar a conversa entre um e outro.',
  'Com o Siga-me, seu Oi redireciona suas chamadas para outro\n'
  + 'telefone fixo ou móvel. Existem vários tipos: quando seu Oi\n'
  + 'estiver ocupado; desligado; quando você não quiser atender\n'
  + 'uma chamada ou mesmo para todas as ligações.',
  'Com a conferência você pode conversar com várias\n'
  + 'pessoas ao mesmo tempo.Pode ser conversa com os\n'
  + 'amigos ou uma reunião de negócios. E é muito\n'
  + 'simples incluir outras pessoas na conversa.\n'
  + 'O serviço é gratuito e você só paga o valor\n'
  + 'de cada ligação que fizer.',
  'O serviço de bloqueio de chamadas 0300, 0500 e 0900\n'
  + 'não deixa que seu Oi faça chamadas para números\n'
  + 'começando com estes prefixos.',
  'O bloqueio de chamadas a cobrar seu Oi não recebe\n'
  + 'chamadas que serão cobradas em sua conta.',
  'O bloqueio de chamadas DDD,\n'
  + 'seu Oi não fará chamadas nacionais.',
  'O bloqueio de chamadas DDI,\n'
  + 'seu Oi não fará chamadas internacionais.',
  'Funciona assim: quando você fizer uma ligação,\n'
  + 'o seu número não irá aparecer no telefone da\n'
  + 'pessoa que irá receber a chamada. Você poderá\n'
  + 'ocultar seu número para algumas pessoas ou para\n'
  + 'todas.'];


let fetchOptions_local;
let itemCombo;

function showServicePage() {
  showCustomerPage()
  $('.js-page-services').show();
  $('.js-massive').hide();
}

function requestService(numerosDependente, numMovel, tipoBundle, ucid, msisdn, fetchOptions) {
  let bundles = ['2P', '2PFB', '3P'];

  fetchOptions_local = fetchOptions;
  codService.sort();
  if (bundles.indexOf(tipoBundle) != -1)
    return showFullMessage('.jsService', 'Cliente não possui Oi Móvel associado.');

  removeFullLoading('.jsService');
  if (numerosDependente) {
    buildCombo(numerosDependente);
  }
  let item = `
    <option value="bota">${numMovel}</option>
    `;
  $('#js-phones').append(item);
}


function buildCombo(numbers) {
  let aux = numbers.map(number => (`
    <option value="bota">${number}</option>
    `));
  $('#js-phones').append(aux);
}

function changeSelect() {
  let comboPhones = document.getElementById('js-phones');
  itemCombo = comboPhones.options[comboPhones.selectedIndex].text;

  if (itemCombo === 'Selecione um móvel') {
    showMsg('');
    return;
  } else {
    showMsg('Aguarde, carregando os Serviços');
  }

  fetch(`${window.ENVS.OI_TOTAL_BACKEND_BASE}/servicos/movel/${itemCombo}?ucid=${ucid}&msisdn=${msisdn}&sessionId=${utilsData.activeSession}`, fetchOptions_local)
    .then(res => res.json())
    .then(json => {
      let cod;
      let servicesCookie = getCookie('services');

      if (!json.length && servicesCookie) {
        json = JSON.parse(servicesCookie);
      } else {
        setCookie('services', JSON.stringify(json));
      }

      cod = json.map(j => j.codigo);
      cod.sort();

      for (let i = 0; i < codService.length; i++) {
        let n = searchCode(codService[i], cod);
        if (n != -1) {
          populaTbody(services[i], json[n].ativo, codService[i], i);
        } else {
          populaTbody(services[i], false, codService[i], i);
        }
      }
    })
    .catch(e => {
      console.error(e)
      showMsg('Não foi possível carregar os serviços! Tente novamente.');
    });

}

function populaTbody(service, ativo, codService, index) {
  let selector;
  let itemTbody = `
        <tr>
            <td class="l-inline-center"><img class="icon" src="images/${ativo ? 'ch' : 'unch'}eckedbox.png"></td>
            <td>${service}</td>
            <td></td>
            <td><button id ='${index}'class="icon-bottom" onclick="confirma(this.id)" value="${codService}">${ativo ? 'Desativar' : 'Ativar'}</button></td>
            <td id='${codService}' class="l-inline-center tooltip-up tooltip-paragraph" data-tooltip="${msgInfo[index]}"><img class="icon" src="images/info.png"></td>
        </tr>
        
    `;
  if (index == 0) {
    $('#serviceContainer tbody').html(itemTbody);
  } else {
    $('#serviceContainer tbody').append(itemTbody);
  }

}

function searchCode(codeSearch, arrayCode) {
  return arrayCode.indexOf(codeSearch);
}

function confirma(id) {
  code = document.getElementById(id).value;
  let conteudo = document.getElementById(id).textContent;
  let features = resolveService(id, conteudo);

  const requestInfo = {
    method: 'POST',
    body: JSON.stringify({numTelOi: itemCombo, codServico: code, acao: features[1]}),
    headers: new Headers({
      'Content-type': 'application/json',
      'pragma': 'no-cache',
      'cache-control': 'no-cache'
    })
  }


  $.confirm({
    title: 'Serviços',
    content: 'Você solicitou a ' + features[0] + ' do Serviço ' + features[2] + ', deseja continuar?',
    buttons: {
      Sim: function () {
        fetch(`${window.ENVS.OI_TOTAL_BACKEND_BASE}/servicos/aprovisionamento/?ucid=${ucid}&msisdn=${msisdn}&sessionId=${utilsData.activeSession}`, requestInfo)
          .then(res => res.json())
          .then(json => json.map(j => alert(j)))
          .catch(e => console.error(e));
        $.alert('Solicitação enviada com sucesso. Aguarde o prazo de 24h para o sistema concluir a ' + features[0]);
      },
      Não: {},

    }
  });
}

function resolveService(index, situation) {
  let customizedSituation;
  let service = services[index];
  let code;
  let features = [];
  if (situation == 'Ativar') {
    customizedSituation = 'ativação';
    code = 'ATV';
  } else {
    customizedSituation = 'desativação';
    code = 'DTV';
  }
  features.push(customizedSituation);
  features.push(code);
  features.push(service);
  return features;
}

function showMsg(msg) {
  $('#serviceContainer tbody').html(`<tr><td colspan="5"><div style="/*min-height: 272px;*/ padding: 124px; text-align: center; font-size: 16px" class="l-text-discreet">${msg}</div></td></tr>`);
}





