(() => {
  'use strict';

  const Automator = {};
  let clientJson = {};
  let fakePhone = '2837212448'; /* development purposes only */

  Automator.client = setClientData;
  Automator.init = init;
  Automator.login = doLogin;
  Automator.show = showPage;
  Automator.populate = populateServices;
  Automator.services = {
    consTerm: {
      title: 'Consulta Terminal',
      button: 'Consulta Terminal',
      loaded: false,
      filled: false
    },
    consServAtivos: {
      title: 'Consultar Serviços Ativos',
      button: 'Serviços Ativos',
      loaded: false,
      filled: false
    },
    consDataInstaLinha: {
      title: 'Consultar Data de Instalação da Linha',
      button: 'Data da Instalação',
      loaded: false,
      filled: false
    },
    detCampInstaVelox: {
      title: 'Detalhamento de Campanha Instalada Velox',
      button: 'Campanha Velox',
      loaded: false,
      filled: false
    }
  };

  // document.addEventListener('DOMContentLoaded', () => {
  //   init();
  // });

  /**
   * Defines client data object
   *
   * @param obj
   */
  function setClientData(obj) {
    clientJson = obj;
  }

  /**
   * Initialize socket connection
   */
  function init() {
    let msgContainer = $('.toolbar--center');
    let button = $('.toolbar-button-automator');

    msgContainer.addClass('minified');
    button.css({display: 'inherit'});

    createButtons();
    getAutomatorData();
  }

  /**
   * Add active class if button content was loaded
   */
  function verifyLoadedServices() {
    for (let button in Automator.services) {
      if (Automator.services.hasOwnProperty(button)) {
        if (Automator.services[button].loaded) {
          $(`.${button}`).addClass('isActive');
        }
      }
    }
  }

  /**
   * Populate services
   *
   * @param service
   */
  function populateServices(service) {
    let data = Automator.services[service];

    if (data && data.loaded) {
      const title = $('.contentWrapper .title');
      const loading = $('.contentWrapper .loading-data');
      const wrapper = $('.dataWrapper');
      const serviceCntr = wrapper.find(`.${service}`);

      wrapper.find('.stc-data').removeClass('isVisible');
      serviceCntr.addClass('isVisible');
      loading.remove();
      title.html(data.title);

      if (!data.filled) {
        if (service !== 'detCampInstaVelox') {
          for (let field in data.data) {
            let fieldTag = $(`.stc-data.${service}`).find(`.${field}`);

            if (typeof data.data[field] === 'string') {
              fieldTag.find('.value').html(data.data[field]);
            } else {
              data.data[field].forEach((value, key) => {
                fieldTag.append(`<span class="items">${value}</span>`);
              })
            }
          }
        } else {
          populateCampanhaVelox(data.data);
        }

        data.filled = true;
      }
    }
  }

  /**
   * Campanha Velox has a different json structure, so
   * it was needed a custom function to populate its data table
   *
   * @param data
   */
  function populateCampanhaVelox(data) {
    const wrapper = $('.dataWrapper').find('.detCampInstaVelox');
    let counter = 0;

    data.forEach((value, key) => {
      let html = '';
      html += '<div class="campanha">';
      html += `  <p class="circuito">Circuito: <span class="value">${value['circuito']}</span></p>`;
      html += `  <p class="produto">Produto: <span class="value"><${value['produto']}/span></p>`;
      html += `  <p class="cliente">Cliente: <span class="value">${value['cliente']}</span></p>`;
      html += `  <p class="campanhas campanha-${counter}">Campanhas: `;
      html += '  </p>';
      html += '</div>';

      wrapper.append(html);

      if (value['campanhas'].length) {
        value['campanhas'].forEach((campanha, i) => {

          let cntr = $('.detCampInstaVelox').find(`.campanha-${counter}`);
          console.log(cntr, campanha);
          cntr.append(`<span class="items">${campanha}</span>`);
        });
      }

      counter ++;
    });
  }

  /**
   * Show automator page
   */
  function showPage() {
    $('.client--personal').appendTo('.js-page-automator .jsClientContainer');
    $('.js-page').hide();
    $('.js-page.js-page-automator').show();
  }

  /**
   * Holds username and password to be used on automator emit string
   */
  function doLogin() {
    let loginCntr = $('.loginWrapper');
    let contentCntr = $('.contentWrapper');
    let username = $('#automatorUser');
    let password = $('#automatorPass');

    if (username.val() && password.val()) {
      window.sessionStorage.setItem('automatorLogin', JSON.stringify({
        username: username.val(),
        password: password.val()
      }));

      loginCntr.removeClass('show');
      contentCntr.addClass('show');
      getAutomatorData();
    }
  }

  /**
   * Make possible for user to login again
   */
  function undoLogin() {
    let loginCntr = $('.loginWrapper');
    let contentCntr = $('.contentWrapper');
    let username = $('#automatorUser');
    let password = $('#automatorPass');

    window.sessionStorage.removeItem('automatorLogin');
    username.val('');
    password.val('');
    loginCntr.addClass('show');
    contentCntr.removeClass('show');
  }

  /**
   * Get automator data
   */
  function getAutomatorData() {
    let loginCntr = $('.loginWrapper');
    let loginInfo = window.sessionStorage.getItem('automatorLogin');
    let clientPhone = normalizePhone(clientJson && clientJson.numFixo || fakePhone);
    let SCTIP;
    let message;
    let fetchData;

    if (loginInfo) {
      loginInfo = JSON.parse(loginInfo);
      SCTIP = getSTCIP(clientJson && clientJson.numFixo || fakePhone);

      if (SCTIP.available) {
        fetchData = [
          SCTIP.ip,
          loginInfo.username,
          loginInfo.password,
          'CAPTURADADOSSTC',
          clientPhone.ddd,
          clientPhone.number
        ].join('/');

        message = 'Aguarde enquanto os dados são carregados...';

        fetch(`${window.ENVS.OI_TOTAL_SOCKET_SERVER}/${fetchData}`, {})
          .then(responseHandler)
          .then(res => res.json())
          .then(json => {
            if (json.error) {
              throw new Error(json.error.message);
            }

            for (let service in json) {
              if (json.hasOwnProperty(service)) {
                Automator.services[service].data = json[service];
                Automator.services[service].loaded = true;
                verifyLoadedServices();
              }
            }

            showMessage('Utilize os botões ao lado para consultar os dados do STC');
          })
          .catch(e => {
            undoLogin();

            $.alert({
              title: 'Erro',
              content: e
            });
          })
      } else {
        message = 'Serviço não está disponível para este DDD';
      }

      showMessage(message);

    } else {
      loginCntr.addClass('show');
    }
  }

  /**
   * Show text message on main card
   *
   * @param message
   */
  function showMessage(message) {
    let contentCntr = $('.contentWrapper');
    let messageCntr = $('.loading-data');

    if (messageCntr.length) {
      messageCntr.remove();
    }

    contentCntr.append(`<p class="loading-data">${message}</p>`);
    contentCntr.addClass('show');
  }

  /**
   * Create Services buttons
   */
  function createButtons() {
    const container = $('.automatorButtons');

    for (let button in Automator.services) {
      if (Automator.services.hasOwnProperty(button)) {
        if (!container.find(`.${button}`).length) {
          $('<a>', {
            text: Automator.services[button].button,
            class: button,
            click: (e) => {
              Automator.populate($(e.currentTarget).attr('class').split(' ')[0]);
            }
          }).appendTo(container);
        }
      }
    }
  }

  /**
   * Get STC IP
   *
   * @param phone
   */
  function getSTCIP(phone) {
    const ddd = phone.slice(0,2);
    const range = phone.slice(0,1);

    if (ddd === '82' || range === '7' || range === '1') {
      return { available: true, ip: '10.31.9.25' };
    } else if (range === '9' || range === '8') {
      return { available: true, ip: '10.31.15.32' };
    } else if (ddd === '28' || ddd === '27' || range === '3') {
      return { available: true, ip: '10.31.9.100' };
    } else if (ddd === '21' || ddd === '22' || ddd === '24') {
      return { available: true, ip: '10.31.15.38' };
    } else {
      return { available: false }
    }
  }

  /**
   * Convert client telephone to the right format
   *
   * @param phone
   * @returns {object}
   */
  function normalizePhone(phone) {
    let ddd = phone.slice(0,2);
    let number = phone.slice(2, phone.length);
    return {
      ddd,
      number
    }
  }

  window.Automator = Automator;
})();
