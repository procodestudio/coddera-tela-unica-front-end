'use strict';

let nota = ['ruim','razoavel','bom','otimo','excelente'];
var notaEscolhida;
var opcao;
var eps;

function choice(id){
    let value = $(`#${id}`).val();
    if(value == 'false'){
        check(id);
        notaEscolhida = id;
    }else{
        noCheck(id);
        notaEscolhida = id;
    }         
}

function check(id){
    $('#js-selectOp').prop('disabled',false);

    for(let i = 0; i<=id; i++){
        $(`#${i}`).addClass('checked');
        $(`#${i}`).val('true');
    }   
}

function noCheck(id){
    for(let i = 4; i>id ; i--){
        $(`#${i}`).removeClass('checked');
        $(`#${i}`).val('false');
    }
}

function choiceOp(val){
    opcao = val; 
    $('#js-selectEps').prop('disabled',false);
}

function choiceEps(value){    
    eps = value;
    $('#js-textArea').prop('disabled',false);
}

function sendInfo(){
    let campo = $('.campo-digitacao');

    let selectOp = document.getElementById('js-selectOp');
    let itemOp = selectOp.options[selectOp.selectedIndex].value;

    let selectEps = document.getElementById('js-selectEps');
    let itemEps = selectEps.options[selectEps.selectedIndex].value;
    
    let text = campo.val();
    if(typeof itemOP == undefined || typeof itemEps == undefined || text == ''){
        $.alert({
                theme: 'light no-overflow',
                title: 'Fale Conosco',
                content: '<div>Preencha todos os campos para que sua pesquisa seja enviada.</div>',
        });       
    }
  
    console.log(text.length);
    console.log(nota[notaEscolhida]);
    console.log(itemOp);
    console.log(itemEps);
    console.log(text);

    const requestInfo ={
        method:'GET',
        body:JSON.stringify({avaliacao:nota[notaEscolhida],operacao:itemOp, eps:itemEps, sugestao:text}),
        headers: new Headers({
          'Content-type':'application/json',
          'pragma': 'no-cache',
          'cache-control': 'no-cache',
        })
    }
   //window.close();
}