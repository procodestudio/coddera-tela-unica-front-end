function requestOS(clientJson, ucid, msisdn, fetchOptions) {
  fetch(`${window.ENVS.OI_TOTAL_BACKEND_BASE}/os?cpf=${clientJson.cpf || ''}&numMovel=${clientJson.numMovel || ''}&numFixo=${clientJson.numFixo || ''}&numContratoTv=${clientJson.numContratoTv || '' || ''}&ucid=${ucid || ''}&msisdn=${msisdn}&sessionId=${utilsData.activeSession}`, fetchOptions)
    .then(responseHandler)
    .then(res => res.json())
    .then(osJson => {
      removeFullLoading('.jsOs.l-relative');
      console.log('JSON:');
      console.log(osJson);
      if (osJson.filter(it => it.possui).length) {
        populateOs({
          jsStatus: `<img class="icon" src="images/${osJson[4].possui ? 'ch' : 'unch'}eckedbox.png">`,
          jsStatusDetail: osJson[4].detalhe,
          jsStatusDate: osJson[4].possui ? '-' : '',
          jsContest: `<img class="icon" src="images/${osJson[0].possui ? 'ch' : 'unch'}eckedbox.png">`,
          jsContestDetail: osJson[0].detalhe,
          jsContestDate: osJson[0].dataAbertura,
          jsRepair: `<img class="icon" src="images/${osJson[1].possui ? 'ch' : 'unch'}eckedbox.png">`,
          jsRepairDetail: osJson[1].detalhe,
          jsRepairDate: osJson[1].possui ? (osJson[1].regiao === 'R1' ? 'Consultar STC' : 'Consultar SAC') : '',
          jsTv: `<img class="icon" src="images/${osJson[2].possui ? 'ch' : 'unch'}eckedbox.png">`,
          jsTvDetail: osJson[2].detalhe,
          jsTvDate: osJson[2].possui ? (osJson[2].regiao === 'R1' ? 'Consultar STC' : 'Consultar SAC') : '',
          jsVeloxFix: `<img class="icon" src="images/${osJson[3].possui ? 'ch' : 'unch'}eckedbox.png">`,
          jsVeloxFixDetail: osJson[3].detalhe,
          jsVeloxFixDate: osJson[3].possui ? 'Consultar STC' : '',
        });
      }
      Monitoring.changeStatus('os');
    })
    .catch(e => {
      console.error('error:', e);
      showFullMessage('.jsOs.l-relative', '<span class="l-error">Não foi poss&iacute;vel realizar a consulta<br/> no momento</span>');
      Monitoring.changeStatus('os');
    });
}
