function requestThermometer(clientJson, ucid, msisdn, fetchOptions) {
  fetch(`${window.ENVS.OI_TOTAL_BACKEND_BASE}/termometro?ucid=${ucid || ''}&msisdn=${msisdn || ''}&sessionId=${utilsData.activeSession}&doc=${clientJson.cpf || ''}&repetidas=${clientJson.repetidas || ''}`, fetchOptions)
    .then(responseHandler)
    .then(res => res.json())
    .then(json => {
      removeFullLoading('.dash.l-relative');

      let retries = clientJson.repetidas;

      if (typeof retries === 'number') {
        if (retries > 1) setDerivativeState('isRed', 'images/smiley-red.png', retries);
        if (retries === 1) setDerivativeState('isYellow', 'images/smiley-yellow.png', retries);
        if (retries === 0) setDerivativeState('isGreen', 'images/smiley-green.png', retries);
        $('.jsRetriesContainer').addClass('isVisible');
      }

      populateThermometerView({
        jsAnatel: json.anatel,
        jsCalls: json.chamadas,
        jsContests: json.contestacao,
        jsJec: json.jec,
        jsRepairs: json.reparo
      });

      $('.dash--thermometer--value').css('position', 'static');
      $('.dash--thermometer--label').css('position', 'static');
      mountThermometerFallback('.js-thermometer-terminal', json.scoreTerminal, json.segmentacaoTerminal);
      mountThermometerFallback('.js-thermometer-client', json.scoreDocumento, json.segmentacaoDocumento);
      Monitoring.changeStatus('thermometer');
    })
    .catch(e => {
      console.error('error:', e);
      showFullMessage('.dash.l-relative', '<span class="l-error">Não foi possível realizar a consulta<br/> no momento</span>');
      Monitoring.changeStatus('thermometer');
    });
}


function translateThermometerColors(color) {
  const colorDictionary = {
    'AMARELO': '#ffcc00',
    'VERMELHO': '#e03633',
    'VERDE': '#5CAA60',
  };
  return colorDictionary[color] || '';
}

function mountThermometerFallback(selector, value, color) {
  const colorDictionary = {
    'AMARELO': '-yellow',
    'VERMELHO': '-red',
    'VERDE': '-green'
  };

  let roundedValue = Math.round(value / 10) * 10;
  let translatedColor = colorDictionary[color];
  $(selector).prepend(`<img class="dash--thermometer--fallback" src="images/thermometers/${roundedValue}${roundedValue ? translatedColor : ''}.png" />`);
  $(`${selector} .dash--thermometer--data--value`).html(`${value}%`).css('color', translateThermometerColors(color));
}
