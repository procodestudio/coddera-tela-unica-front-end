function requestMassiveFailures(clientJson, ucid, msisdn, fetchOptions) {
  let query = `?numFixo=${clientJson.numFixo || ''}&numContrato=${clientJson.contratoTv}&cpfCnpj=${clientJson.cpf}&ucid=${ucid || ''}&msisdn=${msisdn}&sessionId=${utilsData.activeSession}`;
  return fetch(`${window.ENVS.OI_TOTAL_BACKEND_BASE}/massiva${query}`, fetchOptions)
    .then(responseHandler)
    .then(res => res.json())
    .then(massivaJson => {
      removeFullLoading('.massive-failure.l-relative');

      //TV
      if (massivaJson.entity.eventoVulto && massivaJson.entity.eventoVulto.canalAfetado && massivaJson.entity.eventoVulto.canalAfetado.filter(it => it).length) {
        let massiveTv = (massivaJson && massivaJson.entity && massivaJson.entity.eventoVulto) || {canalAfetado: []};
        let affectedChannels = massiveTv.canalAfetado.filter(it => it);
        let expectedSolvingDates = massiveTv.dataPrevisaoSolucao.filter(it => it).map(it => moment(it, 'DD-MM-YYYY HH:mm')).sort((a,b) => a.isBefore(b));
        let promisedDate = expectedSolvingDates[0], now = moment();

        if (promisedDate.isBefore(now)) {
          $('.jsMassiveTv .massive-failure--alert').addClass('isVisible');
        } else {
          $('.jsMassiveTv .massive-failure--alert').removeClass('isVisible');
        }

        populate('.jsMassiveTv', {
          jsPromiseDate: expectedSolvingDates && expectedSolvingDates.length && expectedSolvingDates[0].format('DD/MM/YYYY') || 'Não há',
          jsPromiseHour: expectedSolvingDates && expectedSolvingDates.length && expectedSolvingDates[0].format('HH:mm') || 'Não há',
          jsChannels: affectedChannels.length && '<div class="icon l-clickable"><span class="icon--text">Sim</span><img class="icon--figure" src="images/info.png" alt=""></div>' || 'Não há',
          jsMassiveIcon: `<img class="massive-failure--icon" src="images/${massiveTv.canalAfetado.length ? 'exclamation' : 'exclamation-disabled'}.png"/>`,
        });

        if (affectedChannels.length) {
          $('.jsMassiveTv .jsChannels').on('click', _ => {
            $.alert({
              theme: 'light addons',
              title: 'Canais Afetados',
              content: `
              <table class="table-addon">
                <thead>
                  <tr>
                    <th class="l-text-big">Canal</th>
                    <th class="l-text-big">Data da Promessa</th>
                    <th class="l-text-big">Hora da Promessa</th>
                  </tr>
                </thead>
                <tbody>
                  ${ affectedChannels.map((channel, index) => `
                    <tr>
                      <td>${channel}</td>
                      <td>${expectedSolvingDates[index].format('DD/MM/YYYY')}</td>
                      <td>${expectedSolvingDates[index].format('HH:mm')}</td>
                    </tr>
                    `).join('')
                }
                </tbody>
              </table>
            `
            });
          })
        }
      }

      //Velox
      let massiveFailure = massivaJson.entity || {};
      if (massiveFailure.fixo || massiveFailure.velox) {
        let promisedDate = moment(`${massiveFailure.dataPromessa} ${massiveFailure.horaPromessa}`, 'DD/MM/YYYY HH:mm'),
          now = moment();

        if (promisedDate.isBefore(now)) {
          $('.jsMassiveVelox .massive-failure--alert').addClass('isVisible');
        } else {
          $('.jsMassiveVelox .massive-failure--alert').removeClass('isVisible');
        }

        populate('.jsMassiveVelox', {
          jsPromiseDate: massiveFailure.dataPromessa || 'Não há',
          jsPromiseTime: massiveFailure.horaPromessa || 'Não há',
          jsOsFixo: `<img class="massive-failure--icon" src="images/${massiveFailure.fixo ? 'exclamation' : 'exclamation-disabled'}.png"/>`,
          jsOsVelox: `<img class="massive-failure--icon" src="images/${massiveFailure.velox ? 'exclamation' : 'exclamation-disabled'}.png"/>`,
        });
      }

      Monitoring.changeStatus('massive');
    })
    .catch(e => {
      console.error('error:', e);
      showFullMessage('.massive-failure.l-relative', '<span class="l-error">Não foi possível realizar a consulta<br/> no momento</span>');
      Monitoring.changeStatus('massive');
    });
}
