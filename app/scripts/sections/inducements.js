let clientJson;
let hasFakePhone;
let itemInducement;
let ucid;
let msisdn;

function buildInducementSection(clientJsonGlob, ucidGlob, msisdnGlob, hasFakePhoneGlob){
    clientJson = clientJsonGlob;
    hasFakePhone = hasFakePhoneGlob;
    ucid = ucidGlob;
    msisdn = msisdnGlob;
    removeFullLoading('.jsInducement');
}

function inducement(){
    let comboInducements = document.getElementById('js-inducement');
    itemInducement = comboInducements.options[comboInducements.selectedIndex].value;
    let msg = returnMsgSms(itemInducement);
    showMg(msg);

}

function returnMsgSms(item){
    let str;
    switch(item){
        case '2v':
            str = 'Veja como e simples e facil emitir a sua segunda via de conta na Minha Oi. Saiba mais em http://oi.digital/op-2via';
            return str;
        case 'cb':
            str = 'Acesse o codigo de barra da sua fatura Oi http://oi.digital/cod-moi em caso de duvida, assista aqui como fazer http://oi.digital/op-cod-barras';
            return str;
        case 'ae':
            str = 'Deseja alterar seu endereco de cobranca? Acesse http://oi.digital/op-alteracao-end e modifique de forma rapida as informacoes da sua conta!';
            return str;
        case 'cd':
            str = 'Quer os detalhes da ultima fatura? Acesse http://oi.digital/op-conta-detalhada e veja todas as informacoes da sua Conta Detalhada!';
            return str;
        case 'mo':
            str = 'Nao sabe se cadastrar na Minha Oi? A gente te ajuda! Basta acessar o link http://oi.digital/op-cadastro';
            return str;
        case 'rs':
            str = 'Esqueceu a senha da Minha Oi? A gente te ajuda! Basta acessar http://oi.digital/op-esqueci-senha e alterar de forma rapida e simples!';
            return str;
        default:
            return 0;
            break;
    }
}

function showMg(msg){
    $('#inducementContainer tbody').html(`<tr><td colspan="3" style="border: none; background-color: white;"><div class = "full-element--centered l-text-discreet" style = "padding:15px">${msg}</div></td></tr>`);
}

function sendSMS(){

  if(typeof itemInducement == 'undefined' || itemInducement == ''){
     $.alert('Selecione um incentivo');
     return false;
    }

  let customNumberInput, dialogOptions = [];

    if (hasFakePhone) {
      customNumberInput = `<label class="l-block l-clickable">
             <input class="form--radio" type="radio" name="phone" value="CUSTOM">
             <span>Outro Número: </span>
             <input class="jsInput form--no-spin" placeholder="00000000000" type="number" style="width: 16ch">
           </label>`;
    } else {
      dialogOptions.push({label: 'Titular', phone: clientJson.numMovel});
    }

    if (clientJson.numerosDependente) {
      dialogOptions.push(
        ...clientJson.numerosDependente.map((phone, index) => ({label: `Dep. ${index + 1}`, phone}))
      );
    }

    if (hasFakePhone && clientJson.numeroBinado && clientJson.numeroBinado.length > 10) {
      dialogOptions.push({label: 'Binado', phone: clientJson.numeroBinado});
    }

    const dialogContent = `
      <form class="form jsProtocolForm" onsubmit="return false">
        ${[...dialogOptions.map(op =>
      `<label class="l-block l-clickable">
                 <input class="form--radio" type="radio" name="phone" value="${op.phone}">
                 <span>${op.label}: ${op.phone}</span>
               </label>`),
      customNumberInput
    ].filter(it => it).join('')
      }
      </form>
    `;

    $.confirm({
      title: 'Envio de Incentivo Minha Oi',
      content: dialogContent,
      onContentReady: function () {
        this.$content.find('.jsInput').numeric();
        this.$content.find('.form').on('click', e => {
          const selectedPhone = this.$content.find('form').serializeArray().find(it => it.value);

          if (selectedPhone && selectedPhone.value === 'CUSTOM') {
            this.$content.find('.jsInput').css('display', 'inline-block');
          } else {
            this.$content.find('.jsInput').css('display', 'none');
          }
        })
      },
      buttons: {
        Cancelar: {},
        Enviar: {
          action: function () {
            const selectedPhone = this.$content.find('form').serializeArray().find(it => it.value);

            if (selectedPhone && selectedPhone.value) {

              let destination = selectedPhone.value === 'CUSTOM' ? destination = this.$content.find('.jsInput').val() : selectedPhone.value;

              if (destination.length < 11 || destination.length > 11) {
                $.alert(`O número <strong>${destination}</strong> não possui 11 digitos`);
                return false;
              }

              if (destination[2] != 9) {
                $.alert(`O 3º digito do número <strong>${destination}</strong><br/> deve ser 9`);
                return false;
              }
              let url = `${window.ENVS.OI_TOTAL_BACKEND_BASE}/sms/${destination}?tipo=${itemInducement}&ucid=${ucid}&msisdn=${msisdn}&sessionId=${utilsData.activeSession}`;
              console.log(url);
              fetch(url)
                .then(res => res.json())
                .then(res => {
                  if (res.detail.indexOf('sucesso')){
                    $.alert({
                      title: 'SMS',
                      content: `Incentivo Minha Oi enviado com sucesso por SMS para ${destination}`
                    });
                  } else {
                    $.alert({
                      title: 'SMS',
                      content: `Não foi possível enviar o SMS para o número <strong>${destination}</strong>`
                    });
                  }
                })
            }else{
              $.alert('Selecione um número para envio do incentivo');
              return false;
            }
          }
        }
      }
    });
  }
