(() => {
  'use strict';

  const callData = {};
  const Monitoring = {};
  const services = {
    'addon': { loaded: false },
    'thermometer': { loaded: false },
    'os': { loaded: false },
    'massive': { loaded: false },
    'invoices': { loaded: false }
  };

  Monitoring.setData = setData;
  Monitoring.changeStatus = changeServiceStatus;
  Monitoring.finishMonitoring = finishMonitoring;

  /**
   * Set msisdn and ucid fo later use
   *
   * @param msisdn
   * @param ucid
   */
  function setData(msisdn, ucid) {
    callData.init = Date.now();
    callData.msisdn = msisdn;
    callData.ucid = ucid;
  }

  /**
   * Change servive status
   *
   * @param service
   */
  function changeServiceStatus(service) {
    services[service].loaded = true;
    checkLoadedServices();
  }

  /**
   * Verify if all services has been consumed
   * and call finish monitoring
   */
  function checkLoadedServices() {
    let finishService = true;

    for (let service in services) {
      if (services.hasOwnProperty(service)) {
        if (!services[service].loaded) {
          finishService = false;
        }
      }
    }

    if (finishService) {
      finishMonitoring();
    }
  }

  /**
   * Finish monitoring time line without service check
   */
  function finishMonitoring() {
    let ellapsedTime = Date.now() - callData.init;

    fetch(`${window.ENVS.OI_TOTAL_BACKEND_BASE}/finishTimeline?msisdn=${callData.msisdn}&ucid=${callData.ucid}&ellapsedTime=${ellapsedTime}&sessionId=${utilsData.activeSession}`, {})
  }

  window.Monitoring = Monitoring;
})();
