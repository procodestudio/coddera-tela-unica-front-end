'use strict';

const utilsData = {
  fakeUCID: null,
  activeSession: null,
  activeUCID: null,
  activeMSISDN: null
};

function IE8Optimize() {
  $(document).ready(_ => {
    $('[data-loading]').remove();
  });
}

function runTooltipFallbackIfNeeded() {
  if (bowser.msie && bowser.version <= 8) {
    $('[data-tooltip]').removeClass('tooltip-up');
    $('[data-tooltip]').removeClass('tooltip-paragraph');
    $('[data-tooltip]').each((index, element) => {
      $(element).attr('title', $(element).attr('data-tooltip'));
    });
  }
}

function showFullLoading(selector) {
  $(selector).addClass('l-relative').append(`
    <div class='full-element isVisible jsLoading'>
      <img class="full-element--icon e-rotate isRotating" src="images/loading.png"/>
    </div>  
  `)
}

function showFullMessage(selector, msg) {
  $(selector).addClass('l-relative').append(`
    <div class='full-element isVisible'>
      <div class="full-element--centered l-text-discreet">${msg}</div>
    </div>
  `)
}

function removeFullLoading(selector) {
  if (bowser.msie && bowser.version <= 8) {
    $(selector + ' ' + '.jsLoading').remove();
  } else {
    $(selector + ' ' + '.jsLoading').removeClass('isVisible');
    setTimeout(_ => $(selector + ' ' + '.jsLoading').remove(), 2500);
  }
}

function setupClipboard() {
  try {
    new Clipboard('.js-clipboard');
  } catch (e) {
    //console.warn('Clipboard not loaded');
  }
}

function populateClientView(clientData) {
  Object.keys(clientData).forEach(selector => {
    $(`.client .${selector}`).html(clientData[selector]);
  });
}

function populateThermometerView(data) {
  Object.keys(data).forEach(selector => {
    $(`.dash .${selector}`).html(data[selector]);
  });
}

function populateMassiveVelox(data) {
  Object.keys(data).forEach(selector => {
    $(`.massive-failure .${selector}`).html(data[selector]);
  });
}

function populateOs(data) {
  Object.keys(data).forEach(selector => {
    $(`.jsOs.l-relative .${selector}`).html(data[selector]);
  });
}

function populate(cssScope, data) {
  Object.keys(data).forEach(selector => {
    $(`${cssScope} .${selector}`).html(data[selector]);
  })
}

function setDerivativeState(cssState, iconPath, count) {
  $('.jsRetriesContainer').addClass(cssState);
  $('.jsRetries').addClass(cssState);
  $('.jsRetriesImage').attr('src', iconPath);
  $('.jsRetries').html(`<strong>Repetidas: ${count}</strong>`);
}

function showAlertDialog(msg) {
  $.alert({
    title: 'SMS',
    content: msg,
  });
}

function formatDate(dateStr) {
  if (!dateStr) return '';
  let months = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];
  let match = /(\d{2})-(\w{3})-(\d{2})/.exec(dateStr);
  let month = 0;
  if (match == null) {
      match = /(\d{2})\/(\d{2})\/(\d{2})/.exec(dateStr);
  	  month = parseInt(match[2]) - 1;
      console.log(month);
  } else {
      month = months.indexOf(match[2]);
  }
  let date = new XDate();
  date.setDate(match[1]);
  date.setMonth(month);
  date.setYear(match[3]);
  return date.toString('dd/MM/yyyy');
}

function responseHandler(res) {
  if (!res.ok) throw new Error('Failed to fetch resource');
  return res;
}

function loadImages() {
  $(document).ready(_ => {
    $('[data-src]').each((index, element) => {
      $(element).attr('src', $(element).attr('data-src'));
    });
  });
}

function loadPhone(e) {
  let searchForm = $('#searchForm');
  let msisdn = $('#search').val();
  let queryParams = (queryObject.get() || {});
  let ucid = utilsData.fakeUCID ? utilsData.fakeUCID : queryParams.ucid;

  if (!msisdn || msisdn.length < 10 || msisdn.length > 11) {
    $.alert({
      theme: 'light title',
      title: `<span class="l-text-small">
                <strong>Número Pesquisado:</strong> ${msisdn}
              </span>`,
      content: 'Número inserido não possui 10 ou 11 digitos'
    });
    return false;
  }

  searchForm.find('#searchUCID').val(ucid);
}

function validatePhoneInput(e) {
  let input = $(e.target || e.srcElement);

  let notAllowedChars = [
    46, //.
    43, //+
    45, //-
    44, //,
  ];

  if (notAllowedChars.find(it => it === e.keyCode) || (input.val().toString().length >= 11 && e.keyCode !== 13)) {
    (e.preventDefault && e.preventDefault());
    return false;
  }

  if (e.keyCode === 13 /*Enter Key*/) {
    $('#searchForm').submit();
  }
}

function shouldShowSmsDialog(hasFakePhone, clientJson) {
  return hasFakePhone;
}

function limitChars(e, maximum = 0) {

  let input = $(e.target || e.srcElement);

  if (input.val().length > maximum) {
    e.preventDefault && e.preventDefault();
    input.val(input.val().substring(0, 400));
  }
}

function getCookie(cname) {
  let name = cname + '=';
  let decodedCookie = decodeURIComponent(document.cookie);
  let ca = decodedCookie.split(';');
  for(let i = 0; i <ca.length; i++) {
    let c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return false;
}

function setCookie(cname, cvalue) {
  let d = new Date();
  let exdays = 1;
  d.setTime(d.getTime() + (exdays*24*60*60*1000));
  let expires = 'expires='+ d.toUTCString();
  document.cookie = cname + '=' + cvalue + ';' + expires + ';path=/';
}

function fakeUCID(msisdn) {
  const ts = Date.now();
  const fakeUCID = `${msisdn}_${ts}`;

  utilsData.fakeUCID = fakeUCID;

  return fakeUCID;
}

/* ---------------------------------Lógica fale conosco------------------------------------------*/
{

}

function loadSelect(url){
  const infoRequest ={
    method:'GET',
    mode: 'no-cors'
  }
  return fetch(url)
  .then(response => response.json())
  .catch(e =>{
    console.error(e);
    $.alert({
      theme: 'light no-overflow',
      title: 'Fale Conosco',
      content: '<div>Ocorreram problemas para carregar informações.</div>',
});
  });
}

function faleConosco(){
  window.open('fale_conosco.html', 'faleConosco',
   'status=no, toolbar=no, location=yes,  resizable=no, scrollbars=no, top=10, left=10, width=450, height=650');
}

let nota = ['Ruim','Razoável','Bom','Ótimo','Excelente'];
var notaEscolhida;
var opcao;
var eps;

function loadElements(){
  let url = `${window.ENVS.OI_TOTAL_BACKEND_BASE}`;
  console.log(url+'/motivo');
  console.log(url+'/eps');
  loadSelect(url+'/motivo').then(json=>{
  console.log(json.entity);
  json.entity.map(motivo =>{
     $('#js-selectOp').append(`<option value="${motivo.id}">${motivo.cause}</option>`);
   });
 }).catch(e => console.error(e));

 loadSelect(url+'/eps').then(json=>{
  console.log(json.entity);
  json.entity.map( eps =>{
     $('#js-selectEps').append(`<option value="${eps.id}">${eps.eps}</option>`);
   });
 }).catch(e => console.error(e));

}

function resize(){
  window.resizeTo(450, 650);
}

function choice(id){
    let element = $(`#${id}`);
    let value = element.val();
    if(value == 'false'){
        check(id);
        notaEscolhida = id;
    }else{
        noCheck(id);
        notaEscolhida = id;
    }
    $('.jsEvaluation').text(nota[id]);
    element.blur();
}

function check(id){
    $('#js-selectOp').prop('disabled',false);

    for(let i = 0; i<=id; i++){
        $(`#${i}`).addClass('checked');
        $(`#${i}`).removeClass('noChecked');
        $(`#${i}`).val('true');
    }
}

function noCheck(id){
    for(let i = 4; i>id ; i--){
        $(`#${i}`).removeClass('checked');
        $(`#${i}`).addClass('noChecked');
        $(`#${i}`).val('false');
    }
}

function choiceOp(val){
    opcao = val;
    $('#js-selectEps').prop('disabled',false);
}

function choiceEps(value){
    eps = value;
    $('#js-textArea').prop('disabled',false);
}

function sendInfo(){
  let campo = $('.campo-digitacao');

  let selectOp = document.getElementById('js-selectOp');
  let itemOp = selectOp.options[selectOp.selectedIndex].value;

  let selectEps = document.getElementById('js-selectEps');
  let itemEps = selectEps.options[selectEps.selectedIndex].value;

  let pontuacao = parseInt(notaEscolhida) + 1;

  let text = campo.val();
  if(text == ''){
      $.alert({
              theme: 'light no-overflow',
              title: 'Fale Conosco',
              content: '<div>Preencha todos os campos para que sua pesquisa seja enviada.</div>',
      });
      return;
  }

  console.log(text.length);
  console.log(nota[notaEscolhida]);
  console.log(itemOp);
  console.log(itemEps);
  console.log(text);

  const requestInfo ={
      method:'POST',
      body:JSON.stringify({rating:pontuacao,option:itemOp, eps:itemEps, observation:text}),
      headers: new Headers({
        'Content-type':'application/json',
      })
  }

  fetch(`${window.ENVS.OI_TOTAL_BACKEND_BASE}/fale-conosco`, requestInfo)
  .then(Response.ok)
  .then(res => res.json())
  .then(json =>{
    $('.jsMainPage').hide(),
    $('.jsThanks').html('Obrigado pela sua participação!'),
    $('.jsThanks').show()
  }
  ).catch(e=>{
    console.error(e);
    $('.jsMainPage').hide(),
    $('.jsThanks').html('Não foi possível enviar sua mensagem!'),
    $('.jsThanks').show()
  });

}

