'use strict';

(function () {

  $(document).ready(_ => {
    $('#search').numeric();
  });

  if (bowser.msie && bowser.msie <= 8) {
    IE8Optimize();
  }

  let fetchOptions = {
    headers: {
      'pragma': 'no-cache',
      'cache-control': 'no-cache',
    },
  };

  let queryParams = queryObject.get();
  console.log('Browser query is:', JSON.stringify(queryParams));

  if (!queryParams || !queryParams.msisdn) {
    $(document).ready(_ => $('.e-rotate').remove());
    loadImages();
    return;
  }

  function loadAllData() {
    let msisdn = queryParams.msisdn;
    let ucid = queryParams.ucid || (queryParams.olducid || fakeUCID(msisdn));
    let showErrors = true;

    // Set monitorig data
    utilsData.activeMSISDN = msisdn;
    utilsData.activeUCID = ucid;
    Monitoring.setData(msisdn, ucid);

    return fetch(`${window.ENVS.OI_TOTAL_BACKEND_BASE}/infoCliente?msisdn=${msisdn || ''}&ucid=${ucid || ''}`, fetchOptions)
      .then(responseHandler)
      .then(res => res.json())
      .then(json => {

        let clientJson = (json && json.entity) || {};
        utilsData.activeSession = clientJson.sessionId || Date.now();

        if (json.status == 500) {
          return showClientInvalidMessage('');
        }

        if (clientJson.oiTotal && !clientJson.ativo) {
          showErrors = false;
          Monitoring.finishMonitoring();
          return showClientInvalidMessage('<span class="icon" style="font-size: 18px; font-weight: bold"><span class="icon--text"></span>Cliente Inativo</span>');
        }

        if (!clientJson.oiTotal) {
          showErrors = false;
          Monitoring.finishMonitoring();
          return showClientInvalidMessage('<span class="icon" style="font-size: 18px; font-weight: bold"><span class="icon--text"></span>Cliente não é um Oi Total</span>');
        }

        let hasFakePhone = Boolean(['2P', '2PFB', '3P'].find(it => clientJson.tipoBundle === it));
        clientJson.numerosDependente = clientJson.numerosDependente && clientJson.numerosDependente.filter(it => it);

        removeFullLoading('.client.l-relative');
        requestAddons(clientJson.cpf, ucid, msisdn, fetchOptions);
        requestThermometer(clientJson, ucid, msisdn, fetchOptions);
        requestOS(clientJson, ucid, msisdn, fetchOptions);
        requestMassiveFailures(clientJson, ucid, msisdn, fetchOptions);
        requestInvoices(clientJson, hasFakePhone, ucid, msisdn, fetchOptions);
        requestService(clientJson.numerosDependente, clientJson.numMovel, clientJson.tipoBundle, ucid, msisdn, fetchOptions);
        requestNotes(queryParams.msisdn);
        buildCustomerSection(clientJson, hasFakePhone, ucid, msisdn, fetchOptions);
        buildInducementSection(clientJson, ucid, msisdn, hasFakePhone);
      }).then(res => {
        fetch(`${window.ENVS.OI_TOTAL_BACKEND_BASE}/monitoramento?msisdn=${msisdn || ''}&ucid=${ucid || ''}`, fetchOptions)
          .then(responseHandler)
          .then(res => res.json())
          .then(json => {
            if (json.erro !== '' && showErrors) {
              showErrorMessage('<span class="icon" style="font-size:12px; font-weight: bold">' + json.erro + '</span>');
            }
          });
        })
      .catch(e => {
        console.error('InfoCliente error:', e);
        showErrorMessage('<span class="icon" style="font-size:12px; font-weight: bold">InfoCliente ' + e + '</span>');
        showFullMessage('.dash.l-relative', '<span class="l-error">Não foi possível realizar a consulta<br/> no momento</span>');
        showFullMessage('.jsInvoice', '<span class="l-error">Não foi possível realizar a consulta<br/> no momento</span>');
        showFullMessage('.massive-failure.l-relative', '<span class="l-error">Não foi possível realizar a consulta<br/> no momento</span>');
        showFullMessage('.client.l-relative', '<span class="l-error">Não foi possível realizar a consulta<br/> no momento</span>');
        showFullMessage('.jsOs.l-relative', '<span class="l-error">Não foi possível realizar a consulta<br/> no momento</span>');
        showFullMessage('.jsService', '<span class="l-error">Não foi possível realizar a consulta<br/> no momento</span>');
        Monitoring.finishMonitoring();
      });
  }

  loadAllData();
  loadImages();
})();
