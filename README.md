# OI TELA UNICA FRONTEND

This project aggregates information from clients and displays them 
on a unique screen

## Project choices

This project needs to run on IE8+ and need to be fast, we started it with
react + redux, but we didn't have success for compiling it to es3

So we opted to use some old frinds like **jquery** and webapp generator

## Development cmds

| Command                         | Description                                   |
|---------------------------------|-----------------------------------------------|
| **npm run-script install-all**  | Install node and bower dependencies           |
| **npm run-script build**        | Generate optimized site in 'dist' folder      |
| **gulp**                        | Same as above                                 |
| **npm start**                   | Start development server with cors workaround |
| **gulp serve**                  | Start development server                      |
| **gulp serve:dist**             | Build and serve                               |
